---
home: true
heroImage: /city.png
heroText: MedCore
tagline: Digitalizando a Saúde Brasileira
actionText: Saiba mais →
actionLink: /contato/
features:
  - title: Pacientes
    details: Sua carteira digital de saúde pode ser feita conosco. Tenha em seu bolso, seu histórico de exames assim como comunicação direta com seus médicos.
  - title: Médicos
    details: Comunicação e transparência com seus pacientes ainda pode ser potencializada. Fique ligado para saber mais.
  - title: Instituições de Saúde
    details: O que fazer com a informação no setor da saúde? Fique ligado para saber mais.
footer:
---

<EmailRequester/>
<HomeFooter/>
