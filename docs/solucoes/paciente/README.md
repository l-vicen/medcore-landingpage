## Problema

Usuários de serviço de saúde, pacientes, ainda possuem grande parte de seus exames e prescrições em formato físico. Essa realidade dificulta a comunicação entre diferentes prestadores de serviços de saúde. Em primeiro lugar, porque cada agente tem seu próprio sistema tornando interoperabilidade desafiadora e em segundo lugar, devido ao fato que apesar de inovação no setor de saúde existir, ele ainda é limitado deixando parte da população de lado.

## Visão

Digitalizar os dados de saúde em uma única plataforma - A carteira de saúde digital MedCore.

## Desafios

1. O grande problema ao centralizar dados de saúde é a segurança.
2. Ter uma grande quantidade de usuários.

## Implementação

O processo de digitalização da saúde começa com o paciente, o usuário de nosso produto, e não com os demais agentes da área da saúde já que estes já possuem seus sistemas de administração de históricos de exames. A maneira que vemos a introdução desse produto é a partir da introdução de algumas funcionalidades para o usuários, como: acesso a histórico de saúde digital mobile, agendamento de consultas online, recebimento de receitas em formato digital entre outras features.

O importante é onde o projeto começa, e de acordo com nossa pesquisa, ele deve começar como os pacientes já que estes não possuem uma maneira de arquivar e administrar sua saúde digitalmente.

## Inovação ?

Até então essa descrição não é inovadora uma vez que outros projetos, como Prontuário Eletrônico do SUS, possuem o mesmo proposito. O que indica que é uma direção já considerada no setor, entretanto ainda não completamente funcional. O projeto começa a ficar mais inovador a partir que demais agentes são introduzidos, [Médicos](https://medcore.netlify.app/solucoes/medico/).
