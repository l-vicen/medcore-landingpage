## Problema

A administração de histórico de saúde de um paciente é melhor organizada e acessada, sendo o agente médico uma vez que os registros estão em seus sistemas. Entretanto, muita vezes esse acesso é limitado ao que diz respeito a area de atuação do respectivo agente de saúde. Por exemplo, muitas vezes um cardiologista está atualizado sobre a situação cardiovascular de seu paciente mas não possuí uma contextualização atual do que se passa com o mesmo paciente entre outras áreas de seu corpo.

## Visão

Ter um local onde interoperabilidade entre setores de saúde possa ocorrer de maneira ágil.

## Desafios

1. Tornar o nosso produto "compatível" com o que há no mercado.
2. O grande problema ao centralizar dados de saúde é a segurança.

## Inovação

Interoperabilidade começa a tornar o produto inovador, uma vez que há um serviço que ofereça em escala comunicação digital entre um paciente e diversos médicos que acompanham o paciente em especifico. Entretanto o projeto só alcança seu maior objetivo conectando pacientes e médicos com um terceiro agente, [Instituições de Saúde](https://medcore.netlify.app/solucoes/instituicao/).
