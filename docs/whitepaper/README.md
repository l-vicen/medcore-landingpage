## Pilares

1.  Transparência e Segurança de dados

        "Com a saúde não se brinca." - O desenvolvimento do projeto segue as diretrizes
        das atuais leis de proteção de dados e os participantes do projeto reconhecem a
        importância que carregam, estando dispostos a se posicionarem e comunicarem o
        desenvolvimento do projeto em detalhe através desse whitepaper.

2.  Profissionalismo

        O projeto segue as diretrizes do "Código das Melhores Práticas de Governança
        Corporativa", estabelecidos pelo Instituto Brasileiro de Governança Corporativa,
        para estar em conformidade com seus agentes envolvidos no projeto.

3.  Desenvolvimento ágil e funcional

        Priorizamos a entrega comparado aos detalhes nessa fase.
