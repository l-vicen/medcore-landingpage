module.exports = {
  themeConfig: {
    search: false,
    smoothScroll: true,
    lastUpdated: "Last Updated",
    logo: "logo-roxo.png",
    nav: [
      {
        text: "Home",
        link: "/",
      },
      {
        text: "Soluções",
        items: [
          { text: "Pacientes", link: "/solucoes/paciente/README.md/" },
          { text: "Médicos", link: "/solucoes/medico/README.md/" },
          { text: "Instituições", link: "/solucoes/instituicao/README.md/" },
        ],
      },
      {
        text: "Whitepaper",
        link: "/whitepaper/",
      },
      {
        text: "Contato",
        link: "/contato/",
      },
      {
        link: "/privacidade/",
      },
    ],
    sidebar: ["/", "/page-a", ["/page-b", "Explicit link text"]],
  },
};
